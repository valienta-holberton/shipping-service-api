const API_SERVICES = {
  create: '/autorization/order',
  cancel: '/cancel/order',
  tracking: '/tracking/order',
  rates: '/shipping/rates',
  guides: '/guide/order'
};
