import Router from 'koa-router';
// import { permit } from './middleware/authorizationMiddleware';
// import { RoleEnum } from './utils/enums';
// import hmacMiddleware from './middleware/hmacMiddleware';
import controller = require('./controller/index');

const routes = new Router();

routes.get('/health/ping', controller.health.ping);

// Back-office
// routes.post('/back-office', permit(RoleEnum.ADMIN_USER), controller.invoice.createInvoice);

export default routes;
